/**
 * Copyright (C) 2012 Chris Laplante (MostThingsWeb)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// Based on code by Jared Par, from http://blogs.msdn.com/b/jaredpar/archive/2010/02/19/flattening-class-hierarchies-when-debugging-c.aspx

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

// ReSharper disable CheckNamespace

namespace MostThingsWeb {
    // ReSharper restore CheckNamespace
    /// <summary>
    ///     Specifies a property or field for a BaseMemberPromotingProxy to promote. Multiple attributes can be applied to the same target.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public abstract class BasePromotePropertyAttribute : Attribute {
        private readonly String _name;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BasePromotePropertyAttribute" /> class.
        /// </summary>
        /// <param name="name"> The name of the field or property. </param>
        protected BasePromotePropertyAttribute(String name) {
            _name = name;
        }

        /// <summary>
        ///     Gets or sets the name of the member to promote.
        /// </summary>
        /// <value> The name. </value>
        public String Name {
            get { return _name; }
        }
    }

    /// <summary>
    ///     Specifies a property for a BaseMemberPromotingProxy to promote. Multiple attributes can be applied to the same target.
    /// </summary>
    /// <remarks>
    ///     Use of this attribute causes the BaseMemberPromotingProxy to only promote properties that have been whitelisted with
    ///     this attribute.
    /// </remarks>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public sealed class PromotePropertyAttribute : BasePromotePropertyAttribute {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PromotePropertyAttribute" /> class.
        /// </summary>
        /// <param name="name"> The name of the property. </param>
        public PromotePropertyAttribute(String name)
            : base(name) {}
    }

    /// <summary>
    ///     Specifies a field for a BaseMemberPromotingProxy to promote. Multiple attributes can be applied to the same target.
    /// </summary>
    /// <remarks>
    ///     Use of this attribute causes the BaseMemberPromotingProxy to only promote fields that have been whitelisted with
    ///     this attribute.
    /// </remarks>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public sealed class PromoteFieldAttribute : BasePromotePropertyAttribute {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PromotePropertyAttribute" /> class.
        /// </summary>
        /// <param name="name"> The name of the field. </param>
        public PromoteFieldAttribute(String name)
            : base(name) {}
    }

    /// <summary>
    ///     A debugger type proxy used to "promote" base members of inherited classes so they appear side-by-side
    ///     with members of the active class.
    /// </summary>
    /// <remarks>
    ///     By default, all inherited members are promoted. To change this behavior so that you can whitelist
    ///     individual members (while rejecting others), use the attributes, PromotePropertyAttribute and PromoteFieldAttribute.
    ///     The use of these properties will cause the corresponding member type (property or field) to switch to whitelist-only
    ///     mode.
    /// </remarks>
    public class BaseMemberPromotingProxy {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly object _target;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Member[] _memberList;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseMemberPromotingProxy" /> class.
        /// </summary>
        /// <param name="target"> The target object. </param>
        public BaseMemberPromotingProxy(object target) {
            _target = target;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        internal Member[] Items {
            get { return _memberList ?? (_memberList = BuildMemberList().ToArray()); }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private List<Member> BuildMemberList() {
            var list = new List<Member>();
            if (_target == null) {
                return list;
            }

            const BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var type = _target.GetType();

            // Gather a list of properties and fields to promote from the attributes on the type
            var fieldsToPromote = new HashSet<String>(type.GetCustomAttributes(typeof (PromoteFieldAttribute), true).Select(p => ((PromoteFieldAttribute) p).Name));

            var fields = type.GetFields(flags);
            if (fieldsToPromote.Any()) {
                fields = fields.Where(f => (f.DeclaringType == type || fieldsToPromote.Contains(f.Name))).ToArray();
            }

            var propsToPromote = new HashSet<String>(type.GetCustomAttributes(typeof (PromotePropertyAttribute), true).Select(p => ((PromotePropertyAttribute) p).Name));

            var props = type.GetProperties(flags);
            if (propsToPromote.Any()) {
                props = props.Where(p => (p.DeclaringType == type || propsToPromote.Contains(p.Name))).ToArray();
            }

            // Process fields
            list.AddRange(fields.Where(IsMemberBrowsable).Select(field => new Member(field.Name, field.GetValue(_target), field.FieldType)));

            // Process properties (index parameter check is necessary to ignore indexers)
            foreach (var prop in props.Where(prop => prop.GetIndexParameters().Length <= 0 && IsMemberBrowsable(prop))) {
                object value;
                try {
                    value = prop.GetValue(_target, null);
                } catch (Exception ex) {
                    value = ex;
                }
                list.Add(new Member(prop.Name, value, prop.PropertyType));
            }

            return list.OrderBy(m => m.Name).ToList();
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private static bool IsMemberBrowsable(MemberInfo memberInfo) {
            return memberInfo.GetCustomAttributes(typeof (DebuggerBrowsableAttribute), true).Select(p => (DebuggerBrowsableAttribute) p).All(p => p.State != DebuggerBrowsableState.Never);
        }

        [DebuggerDisplay("{Value}", Name = "{Name,nq}", Type = "{Type.ToString(),nq}")]
        internal struct Member {
            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            internal string Name;

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            internal Type Type;

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            internal object Value;

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            internal Member(string name, object value, Type type) {
                Name = name;
                Value = value;
                Type = type;
            }
        }
    }
}